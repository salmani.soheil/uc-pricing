import React, { Component } from 'react';
import LoadStepper from './components/LoadStepper';
import NavBar from './components/NavBar';

class App extends Component {
  render() {
    return (
      <div>
        <NavBar title="Pricing App" />
        <LoadStepper />
      </div>
    )
  }
}

export default App;
