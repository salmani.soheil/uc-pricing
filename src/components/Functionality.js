import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ProductSlider from '../components/ProductSlider';
import TabPanel from '../components/TabPanel';
import SubstitutionsGrid from '../components/SubstitutionsGrid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import GetAppIcon from '@material-ui/icons/GetApp';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
  root: {
    flexGrow: 1,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttons: {
    marginTop: theme.spacing(2),
    textAlign: 'center'
  },
  button: {
    marginLeft: theme.spacing(1),
  }
});

class Functionality extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      products: props.products,
      substitutions: props.substitutions,
      results: [],
      nextDisabled: false,
      backDisabled: true
    }
    this.updateResults = this.updateResults.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleNext = this.handleNext.bind(this);
  }

  getSubstitutions = (currentProduct, products, substitutions) => {
    let currentBarcode = currentProduct.barcode;
    let currentSubs = substitutions.find(s => s.barcode === currentBarcode).matchs;

    let targetProduct = this.state.results.filter(p => p.barcode === currentProduct.barcode)
    targetProduct = targetProduct && targetProduct[0]

    let res = currentSubs.map(subBarcode => {
      let sub = {}
      console.log(this.state.results);
      console.log(currentProduct); 
      let product = products.find(p => p.barcode === subBarcode);
      
      let targetSub = targetProduct && targetProduct.substitutions.filter(s => s.barcode === subBarcode)
      targetSub = targetSub && targetSub[0]
      let status = targetSub && targetSub.status;

      sub.status = status || 'UNKNOWN';
      sub.barcode = product.barcode;
      sub.urls = product.urls;
      return sub
    });
  
    return res;
  };

  handleNext() {
    if (this.state.index === (this.state.substitutions.length - 1)) { return; }
    this.setState({index: this.state.index + 1}, () => {
      if (this.state.index === (this.state.substitutions.length - 1)) {
        this.setState({nextDisabled: true});
      } else {
        this.setState({nextDisabled: false});
      }
      if (this.state.index === 0) {
        this.setState({backDisabled: true});
      } else {
        this.setState({backDisabled: false});
      }
    });
  }

  handleBack() {
    if (this.state.index === 0) { return; }
    this.setState({index: this.state.index - 1}, () => {
      if (this.state.index === (this.state.substitutions.length - 1)) {
        this.setState({nextDisabled: true});
      } else {
        this.setState({nextDisabled: false});
      }
      if (this.state.index === 0) {
        this.setState({backDisabled: true});
      } else {
        this.setState({backDisabled: false});
      }
    });
  }

  updateResults(product, substitutions) {
    let results = this.state.results.filter(elt => elt.barcode !== product.barcode)
    results.push({
      barcode: product.barcode,
      substitutions: substitutions.map((s) => {
        return {
          barcode: s.barcode,
          status: s.status
        };
      })
    });
    this.setState({ results }, () => {console.log(this.state.results)});
  }

  render() {
    const { classes, theme } = this.props;
    console.log(this.state.products);
    return (
      <div className={classes.root}>
        <Grid container spacing={3} justify="space-evenly" alignItems='stretch'>
          <Grid item xs={4}>
            <Typography variant="h4" style={{textAlign: 'center', marginTop: 20, marginBottom: 20}}>Produit</Typography>
            <ProductSlider product={this.state.products[this.state.index]} index={this.state.index + 1} total={this.state.substitutions.length} />
            <div className={classes.buttons}>
              <Button className={classes.button} disabled={this.state.backDisabled} variant="contained" onClick={this.handleBack}>Précédent</Button>
              <Button className={classes.button} disabled={this.state.nextDisabled} variant="contained" color="primary" onClick={this.handleNext}>Suivant</Button>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                startIcon={<GetAppIcon />} download="results.json" href={"data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.state.results, null, 4))}
              >Télécharger</Button>
            </div>
            <Button />
          </Grid>
          <Grid item xs={8}>
            <Typography variant="h4" style={{textAlign: 'center', marginTop: 20, marginBottom: 20}}>Produits à labelliser</Typography>
            <TabPanel />
            <SubstitutionsGrid updateResults={this.updateResults} product={this.state.products[this.state.index]} substitutions={this.getSubstitutions(this.state.products[this.state.index], this.state.products, this.state.substitutions)} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Functionality);
