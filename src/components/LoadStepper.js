import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CodeSnippet from '../components/CodeSnippet';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Functionality from '../components/Functionality';
import TabPanel from '../components/TabPanel';

const classes = theme => ({
  root: {
    width: '100%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
});

const jsonProductsFormat = `[
  {
    "barcode": "<product-barcode>",
    "url": [
      "<product-img-url>",
      "<product-img-url>",
      ...
    ]
  },
  ...
}`;

const jsonSubstitutionsFormat = `[
  {
    "barcode": "<product-barcode>",
    "matchs": [
      "<substitute-product-barcode>",
      "<substitute-product-barcode>"
      ...
    ]
  },
  ...
}`;

function getSteps() {
  return ['Importer les images des produits', 'Importer les produits à labelliser'];
}

const steps = getSteps();

class LoadStepper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      products: [],
      substitutions: []
    };
    this.steps = steps;
  }

  handleNext = (event) => {
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.onload = event => {
      let result = JSON.parse(event.target.result);
      if (this.state.activeStep == 0) {
        this.state.products = result;
      } else {
        this.state.substitutions = result;
      }
      this.setState({ activeStep: this.state.activeStep + 1});
    };

    reader.readAsText(file);
  };

  handleBack = () => {
    this.setState({ activeStep: this.state.activeStep - 1});
  };

  handleReset = () => {
    this.setState({ activeStep: 0 });
  };

  getStepContent(step) {
    switch (step) {
      case 0:
        return <CodeSnippet code={jsonProductsFormat} language='json' />
      case 1:
        return (
          <>
            {/* <Typography variant="h6" style={{marginTop: 20, marginBottom: 20}}>Choisir le type de labellisation</Typography> */}
            {/* <TabPanel /> */}
            {/* <Typography variant="h6" style={{marginTop: 20, marginBottom: 20}}>Schéma du fichier JSON à importer</Typography> */}
            <CodeSnippet code={jsonSubstitutionsFormat} language='json' />
          </>
        );
      case 2:
        return (
          <p>
            <Typography>Vous avez importé 18324 produits.</Typography>
          </p>
        );
      default:
        return 'Unknown step';
    }
  }

  render() {
    return (
      <div className={classes.root}>
        <Stepper activeStep={this.state.activeStep} orientation="vertical">
          {this.steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                <Typography>{this.getStepContent(index)}</Typography>
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={this.state.activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      Retour
                    </Button>
                    <Button
                      variant="contained"
                      color="default"
                      className={classes.button}
                      startIcon={<CloudUploadIcon />}
                      component="label"
                    >
                      Importer JSON
                      <input
                        type="file"
                        style={{ display: "none" }}
                        onChange={this.handleNext}
                      />
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {this.state.activeStep === this.steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Button onClick={this.handleReset} className={classes.button}>
              Recommencer
            </Button>
            <Divider />
            <Functionality products={this.state.products} substitutions={this.state.substitutions} />
          </Paper>
        )}
      </div>
    );
  }
}

LoadStepper.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(classes)(LoadStepper);
