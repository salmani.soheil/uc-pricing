import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    height: 520
  },
  img: {
    height: '100%',
    width: '100%',
    objectFit: 'contain',
    padding: '1rem'
  }
}));

export default function ProductSlider(props) {
  const classes = useStyles();

  const [product, setProduct] = React.useState(props.product);

  return (
    <Paper className={classes.root} elevation={6}>
      {/* <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          className={classes.media}
          image={props.product.urls[0]}
          title={props.product.barcode}
        /> */}
        <img src={props.product.urls[0]} alt='Produit' className={classes.img} />
        {/* <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {`${props.product.barcode} (${props.index}/${props.total})`}
          </Typography>
        </CardContent> */}
      {/* </CardActionArea> */}
    </Paper>
  );
}