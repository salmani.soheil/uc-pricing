import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import Chip from '@material-ui/core/Chip';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Button from '@material-ui/core/Button';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import HelpIcon from '@material-ui/icons/Help';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    transform: 'translateZ(0)',
    width: "100%",
    // display: 'flex',
    // flexDirection: 'column'
  },
  titleBar: {
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
      'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  icon: {
    color: 'white',
  },
  img: {
    height: '100%',
    width: '100%',
    objectFit: 'contain',
  },
  actionButtons: {
    backgroundColor: '#e0e0e0',
    display: 'flex',
    justifyContent: 'center'
  },
  yesButton: {
    boxSizing: 'content-box',
    display: 'inline-block',
    padding: '0.5rem',
    color: '#4caf50'
  },
  yesButtonActive: {
    boxSizing: 'content-box',
    backgroundColor: '#4caf50',
    padding: '0.5rem',
    borderRadius: '50%',
    color: 'white'
  },
  noButton: {
    boxSizing: 'content-box',
    display: 'inline-block',
    padding: '0.5rem',
    color: '#d50000'
  },
  noButtonActive: {
    boxSizing: 'content-box',
    backgroundColor: '#d50000',
    padding: '0.5rem',
    borderRadius: '50%',
    color: 'white'
  },
  nullButton: {
    boxSizing: 'content-box',
    display: 'inline-block',
    padding: '0.5rem',
    color: '#757575'
  },
  nullButtonActive: {
    boxSizing: 'content-box',
    backgroundColor: '#757575',
    padding: '0.5rem',
    borderRadius: '50%',
    color: 'white'
  }
});

class SubstitutionsGrid extends Component {

  constructor(props) {
      super(props);
      this.state = {
        substitutions: props.substitutions 
      }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.product !== this.props.product) {
      this.setState({ substitutions: this.props.substitutions });
      this.props.updateResults(prevProps.product, prevState.substitutions);
    }
  }

  toggleSub = (e, i, newState) => {
    if (this.state.substitutions[i].status === newState) {
        newState = 'UNKNOWN';
    }
    let newSubs = this.state.substitutions;
    newSubs[i].status = newState;
    this.setState({ substitutions: newSubs }, () => console.log(this.state.substitutions));
  };

  render() {
    const { classes, theme } = this.props;
    return (    
      <div className={classes.root}>
        {/* <GridList spacing={24} className={classes.gridList} cols={3}>
          {this.state.substitutions.map((tile, index) => (
            <GridListTile key={`tile-{index}`} className={classes.gridListTile} cols={1} rows={2} className={classes.gridListTile} elevation={6}>
              <img src={tile.urls[0]} alt={tile.barcode} className={classes.img} />
              <GridListTileBar
                title={<Chip label={tile.barcode} />}
                titlePosition="top"
                actionIcon={
                  <IconButton aria-label={`star ${tile.barcode}`} className={classes.icon} onClick={e => this.toggleSub(e, index)}>
                    {this.state.substitutions[index].checked ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />}
                  </IconButton>
                }
                actionPosition="bottom"
                className={classes.titleBar}
              />
            </GridListTile>
          ))}
        </GridList> */}
        <Grid container spacing={3}>
          {this.state.substitutions.map((tile, index) => (
            <Grid item xs={3}>
              <Paper elevation={6}>
                <img src={tile.urls[0]} alt={tile.barcode} className={classes.img} />
                <div className={classes.actionButtons}>
                  <IconButton style={{color: '#4caf50'}} aria-label="YES" className={classes.margin}>
                    {this.state.substitutions[index].status === 'YES' ? <ThumbUpIcon fontSize="large" className={classes.yesButtonActive} onClick={e => this.toggleSub(e, index, 'YES')} /> : <ThumbUpIcon fontSize="large" className={classes.yesButton} onClick={e => this.toggleSub(e, index, 'YES')} />}
                  </IconButton>
                  <IconButton style={{color: '#d50000'}} aria-label="NO" className={classes.margin}>
                    {this.state.substitutions[index].status === 'NO' ? <ThumbDownIcon fontSize="large" className={classes.noButtonActive} onClick={e => this.toggleSub(e, index, 'NO')} /> : <ThumbDownIcon fontSize="large" className={classes.noButton} onClick={e => this.toggleSub(e, index, 'NO')}/>}
                  </IconButton>
                  <IconButton aria-label="NULL" className={classes.margin}>
                    {this.state.substitutions[index].status === 'UNKNOWN' ? <HelpIcon fontSize="large" className={classes.nullButtonActive} onClick={e => this.toggleSub(e, index, 'UNKNOWN')} /> : <HelpIcon fontSize="large" className={classes.nullButton} onClick={e => this.toggleSub(e, index, 'UNKNOWN')} />}
                  </IconButton>
                </div>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SubstitutionsGrid);